#install.packages('tsintermittent')
#install.packages('ggplot2')
#install.packages('reshape2')
#install.packages('pracma')

library(tsintermittent)
library(ggplot2)
library(reshape2)
library(pracma)

#Setting the working directory (Change to your working directory)
setwd("C:/Strata/Data")


#*****************LOADING DATA FILES*******
sku1<-read.csv(file="sku1.csv",
               colClasses=c("character", "numeric"))

sku2<-read.csv(file="sku2.csv",
               colClasses=c("character", "numeric"))

sku3<-read.csv(file="sku3.csv",
               colClasses=c("character", "numeric"))




#****************TIME PLOTS OF VARIOUS SKU's****

sku<-cbind(sku1,sku2[,2],sku3[,2])
sku$Date<-as.POSIXct(sku$Date,format="%m/%d/%Y")

names(sku)<-c("Date","sku1","sku2","sku3")

sku_long<-melt(data=sku,id.vars="Date")

timeplot<-ggplot()+
  geom_path(data=sku_long,
            aes(x=Date,y=value,color=variable))+
  facet_grid(variable~.,scale="free_y")

timeplot


#***********Function to compute MAPE**********

#Function to compute MAPE for a DataFrame
computemape_df<-function(forecast,actual){
  mape<-abs(rowSums(forecast,na.rm=T,dims=1)-rowSums(actual,na.rm=T,dims=1))/
    rowSums(actual,na.rm=T,dims=1)*100
  return(mape)
}

#Function to compute MAPE for a List
computemape_list<-function(forecast,actual){
  mape<-abs(sum(forecast)-sum(actual))/sum(actual)*100
  return(mape)
}



#******************Sample Demonstration of Running Croston Method*********


#*******************Applying Croston Method on sku3

croston_sku <- crost(data=sku3[1:545,2],h=5,init.opt=1,nop=1,type="croston",cost="mae")

#Output for the forecast
croston_sku$components$c.out #Gives Demand Size and Inter Arrival Time
croston_sku$frc.out #Gives Demand Rate

prediction_crost <- croston_sku$frc.out
actual_demand <- sku3[546:550,2]

#Computing the MAPE
computemape_list(prediction_crost,actual_demand)


#**********Comparitive analysis between SMA, Exponential Smoothing, ARIMA and croston Method


#Generating forecast based on Simple Moving Average

myforecast_sma<-function(skudata){
  tfcst<-list()
  fcst<-matrix(nrow=6,ncol=10)
  for (i in 1:6){
    sku_ts <- ts(skudata[1:(544+i),2],frequency = 6)
    tfcst[[i]]<-sma(sku_ts,h=5)
    predictSMA_sku <- forecast(tfcst[[i]], h = 5 , prediction.interval = T, level = 0.95)
    fcst[i,i:(i+4)]<-predictSMA_sku$mean
  }
  
  actual<-matrix(nrow=6,ncol=10)
  for (i in 1:6){
    actual[i,i:(i+4)]<-skudata$Demand[(545+i):(549+i)]
  }
  
  mape<-computemape_df(fcst,actual)
  
  cforecast<-list(fcst,tfcst,mape,actual)
  names(cforecast)<-c("fcst","fcstdetails","mape","actual")
  
  return(cforecast)
}




#Generating forecast based on Exponential Smooting

myforecast_ets<-function(skudata){
  tfcst<-list()
  fcst<-matrix(nrow=6,ncol=10)
  for (i in 1:6){
    sku_ts <- ts(skudata[1:(544+i),2],frequency = 6)
    tfcst[[i]]<-ets(sku_ts)
    predictETS_sku <- forecast(tfcst[[i]], h = 5 , prediction.interval = T, level = 0.95)
    fcst[i,i:(i+4)]<-predictETS_sku$mean
  }
  
  actual<-matrix(nrow=6,ncol=10)
  for (i in 1:6){
    actual[i,i:(i+4)]<-skudata$Demand[(545+i):(549+i)]
  }
  
  mape<-computemape_df(fcst,actual)
  
  cforecast<-list(fcst,tfcst,mape,actual)
  names(cforecast)<-c("fcst","fcstdetails","mape","actual")
  
  return(cforecast)
}



#Generating forecast based on ARIMA


myforecast_arima<-function(skudata){
  tfcst<-list()
  fcst<-matrix(nrow=6,ncol=10)
  for (i in 1:6){
    sku_ts <- ts(skudata[1:(544+i),2],frequency = 6)
    tfcst[[i]]<-auto.arima(sku_ts, ic=c("aicc","aic","bic"))
    predictArima_sku <- forecast(tfcst[[i]], h = 5 , prediction.interval = T, level = 0.95)
    fcst[i,i:(i+4)]<-predictArima_sku$mean
  }
  
  actual<-matrix(nrow=6,ncol=10)
  for (i in 1:6){
    actual[i,i:(i+4)]<-skudata$Demand[(545+i):(549+i)]
  }
  
  mape<-computemape_df(fcst,actual)
  
  cforecast<-list(fcst,tfcst,mape,actual)
  names(cforecast)<-c("fcst","fcstdetails","mape","actual")
  
  return(cforecast)
}




#********Applying Croston by varying the cost functions and model types***********

#Determining the optimal parameters for croston

types<-c("croston","sba","sbj")
costs<-c("mar","msr","mae","mse")

#Iterating to determine MAPE for each type and cost
findmape<-function(skudata){
  mapetable<-matrix(nrow=12,ncol=8)
  id<-0
  for (j in 1:length(types)){
    for (k in 1:length(costs)){
      tfcst<-data.frame()
      fcst<-matrix(nrow=6,ncol=10)
      for (i in 1:6){
        tfcst<-crost(data=skudata[1:(544+i),2], 
                     h=5,
                     init.opt=1,
                     nop=1,
                     type=types[j],
                     cost=costs[k])
        fcst[i,i:(i+4)]<-tfcst$frc.out
      }
      
      actual<-matrix(nrow=6,ncol=10)
      for (i in 1:6){
        actual[i,i:(i+4)]<-skudata$Demand[(545+i):(549+i)]
      }
      
      id<-id+1
      mapetable[id,1:6]<-computemape_df(fcst,actual)
      mapetable[id,7]<-types[j]
      mapetable[id,8]<-costs[k]
    }
  }
  return(mapetable)
}



#Generating forecast based on Croston Method

myforecast_croston<-function(skudata,usetype,usecost){
  tfcst<-list()
  fcst<-matrix(nrow=6,ncol=10)
  for (i in 1:6){
    tfcst[[i]]<-crost(data=skudata[1:(544+i),2], 
                      h=5,
                      init.opt=1,
                      nop=1,
                      type=usetype,
                      cost=usecost)
    fcst[i,i:(i+4)]<-tfcst[[i]]$frc.out
  }
  
  actual<-matrix(nrow=6,ncol=10)
  for (i in 1:6){
    actual[i,i:(i+4)]<-skudata$Demand[(545+i):(549+i)]
  }
  
  mape<-computemape_df(fcst,actual)
  
  cforecast<-list(fcst,tfcst,mape,actual)
  names(cforecast)<-c("fcst","fcstdetails","mape","actual")
  
  return(cforecast)
}


#*************Finding Forecast and MAPE using Various Methods


Forecast_SMA<-function(sku){
  #Forecasting using Simple Moving Average
  fcstsku_SMA<-myforecast_sma(sku)
  
  
  forecast_sma_sku<-as.data.frame(fcstsku_SMA$fcst)
  colnames(forecast_sma_sku)<-c("Forecast_Day1","Forecast_Day2","Forecast_Day3","Forecast_Day4",
                                 "Forecast_Day5","Forecast_Day6","Forecast_Day7","Forecast_Day8",
                                 "Forecast_Day9","Forecast_Day10")
  
  forecast_sma_sku['Total_Forecasted_Demand']<-round(rowSums(forecast_sma_sku,na.rm=T),0)
  
  
  actual_sku<-as.data.frame(fcstsku_SMA$actual)
  colnames(actual_sku)<-c("Actual_Day1","Actual_Day2","Actual_Day3","Actual_Day4",
                           "Actual_Day5","Actual_Day6","Actual_Day7","Actual_Day8",
                           "Actual_Day9","Actual_Day10")
  
  actual_sku['Total_Actual_Demand']<-round(rowSums(actual_sku,na.rm=T),0)
  
  
  fcstsku_mape_SMA <- as.data.frame(fcstsku_SMA$mape)
  fcstsku_mape_SMA<-as.data.frame(t(fcstsku_mape_SMA))
  row.names(fcstsku_mape_SMA)<-"SMA"
  colnames(fcstsku_mape_SMA)<-c("MAPE_Window1","MAPE_Window2","MAPE_Window3","MAPE_Window4",
                                 "MAPE_Window5","MAPE_Window6")
  fcstsku_mape_SMA['Mean_MAPE']<-rowMeans(fcstsku_mape_SMA[c("MAPE_Window1","MAPE_Window2","MAPE_Window3","MAPE_Window4",
                                                               "MAPE_Window5","MAPE_Window6")])
  
  
  
  fcstsku<-list(forecast_sma_sku,actual_sku,fcstsku_mape_SMA)
  names(fcstsku)<-c("forecast","actual","mape")
  
  return(fcstsku)
  
}


Forecast_ETS<-function(sku){
  #Forecasting using Exponential Smoothing Method
  fcstsku_ETS<-myforecast_ets(sku)
  
  
  forecast_ets_sku<-as.data.frame(fcstsku_ETS$fcst)
  colnames(forecast_ets_sku)<-c("Forecast_Day1","Forecast_Day2","Forecast_Day3","Forecast_Day4",
                                 "Forecast_Day5","Forecast_Day6","Forecast_Day7","Forecast_Day8",
                                 "Forecast_Day9","Forecast_Day10")
  
  forecast_ets_sku['Total_Forecasted_Demand']<-round(rowSums(forecast_ets_sku,na.rm=T),0)
  
  
  fcstsku_mape_ETS <- as.data.frame(fcstsku_ETS$mape)
  fcstsku_mape_ETS<-as.data.frame(t(fcstsku_mape_ETS))
  row.names(fcstsku_mape_ETS)<-"Exponential Smoothing"
  colnames(fcstsku_mape_ETS)<-c("MAPE_Window1","MAPE_Window2","MAPE_Window3","MAPE_Window4",
                                 "MAPE_Window5","MAPE_Window6")
  fcstsku_mape_ETS['Mean_MAPE']<-rowMeans(fcstsku_mape_ETS[c("MAPE_Window1","MAPE_Window2","MAPE_Window3","MAPE_Window4",
                                                               "MAPE_Window5","MAPE_Window6")])
  
  
  fcstsku<-list(forecast_ets_sku,fcstsku_mape_ETS)
  names(fcstsku)<-c("forecast","mape")
  
  return(fcstsku)
}



Forecast_ARIMA<-function(sku){
  #Forecasting using Arima Method
  fcstsku_arima<-myforecast_arima(sku)
  
  
  forecast_arima_sku<-as.data.frame(fcstsku_arima$fcst)
  colnames(forecast_arima_sku)<-c("Forecast_Day1","Forecast_Day2","Forecast_Day3","Forecast_Day4",
                                   "Forecast_Day5","Forecast_Day6","Forecast_Day7","Forecast_Day8",
                                   "Forecast_Day9","Forecast_Day10")
  
  forecast_arima_sku['Total_Forecasted_Demand']<-round(rowSums(forecast_arima_sku,na.rm=T),0)
  
  
  
  fcstsku_mape_arima <- as.data.frame(fcstsku_arima$mape)
  fcstsku_mape_arima<-as.data.frame(t(fcstsku_mape_arima))
  row.names(fcstsku_mape_arima)<-"ARIMA"
  colnames(fcstsku_mape_arima)<-c("MAPE_Window1","MAPE_Window2","MAPE_Window3","MAPE_Window4",
                                   "MAPE_Window5","MAPE_Window6")
  fcstsku_mape_arima['Mean_MAPE']<-rowMeans(fcstsku_mape_arima[c("MAPE_Window1","MAPE_Window2","MAPE_Window3","MAPE_Window4",
                                                                   "MAPE_Window5","MAPE_Window6")])

  fcstsku<-list(forecast_arima_sku,fcstsku_mape_arima)
  names(fcstsku)<-c("forecast","mape")
  
  return(fcstsku)
  
}



optimal_Croston<-function(sku){
  #Running the iterations for SKU's
  ptm <- proc.time()
  
  mapetable1<-findmape(sku)
  colnames(mapetable1)<-c("MAPE_Window1","MAPE_Window2","MAPE_Window3","MAPE_Window4",
                          "MAPE_Window5","MAPE_Window6","Method","Cost_Function")
  proc.time() - ptm
  
  
  mapetable1<-as.data.frame(mapetable1)
  
  mapetable1$MAPE_Window1<-as.numeric(as.character(mapetable1$MAPE_Window1))
  mapetable1$MAPE_Window2<-as.numeric(as.character(mapetable1$MAPE_Window2))
  mapetable1$MAPE_Window3<-as.numeric(as.character(mapetable1$MAPE_Window3))
  mapetable1$MAPE_Window4<-as.numeric(as.character(mapetable1$MAPE_Window4))
  mapetable1$MAPE_Window5<-as.numeric(as.character(mapetable1$MAPE_Window5))
  mapetable1$MAPE_Window6<-as.numeric(as.character(mapetable1$MAPE_Window6))
  
  
  #Computing the Average MAPE
  mapetable1['Average_MAPE']<-rowMeans(mapetable1[c("MAPE_Window1","MAPE_Window2","MAPE_Window3","MAPE_Window4",
                                                    "MAPE_Window5","MAPE_Window6")])
  return(mapetable1)
}



Forecast_Croston<-function(sku,type,cost){
  #Forecasting using Croston Method
  fcstsku_croston<-myforecast_croston(sku,usetype=type,usecost=cost)
  forecast_croston_sku<-as.data.frame(fcstsku_croston$fcst)
  colnames(forecast_croston_sku)<-c("Forecast_Day1","Forecast_Day2","Forecast_Day3","Forecast_Day4",
                                    "Forecast_Day5","Forecast_Day6","Forecast_Day7","Forecast_Day8",
                                    "Forecast_Day9","Forecast_Day10")
  
  forecast_croston_sku['Total_Forecasted_Demand']<-round(rowSums(forecast_croston_sku,na.rm=T),0)
  
  fcstsku_mape_croston <- as.data.frame(fcstsku_croston$mape)
  fcstsku_mape_croston<-as.data.frame(t(fcstsku_mape_croston))
  row.names(fcstsku_mape_croston)<-"Croston"
  colnames(fcstsku_mape_croston)<-c("MAPE_Window1","MAPE_Window2","MAPE_Window3","MAPE_Window4",
                                     "MAPE_Window5","MAPE_Window6")
  fcstsku_mape_croston['Mean_MAPE']<-rowMeans(fcstsku_mape_croston[c("MAPE_Window1","MAPE_Window2","MAPE_Window3","MAPE_Window4",
  
                                                                                                                                          "MAPE_Window5","MAPE_Window6")])
  fcstsku<-list(forecast_croston_sku,fcstsku_mape_croston)
  names(fcstsku)<-c("forecast","mape")
  
  return(fcstsku)
}



#**********Forecasting and Computing MAPE for Various SKU's*********

#*****SKU1*****
#Foreasting sku1 using simple moving average, Exponential Smoothing, ARIMA and Croston Method
forecast_SMA_sku1<-Forecast_SMA(sku1)
forecast_ETS_sku1<-Forecast_ETS(sku1)
forecast_ARIMA_sku1<-Forecast_ARIMA(sku1)
#optimal_crost_sku1<-optimal_Croston(sku1) #For finding optimal croston parameters
forecast_Croston_sku1<-Forecast_Croston(sku1,"croston","mar")


#Getting the actuals and forecasted value dataframes
actualsku1<-forecast_SMA_sku1$actual
predicted_SMA_sku1<-forecast_SMA_sku1$forecast
predicted_ETS_sku1<-forecast_ETS_sku1$forecast
predicted_ARIMA_sku1<-forecast_ARIMA_sku1$forecast
predicted_Croston_sku1<-forecast_Croston_sku1$forecast


#Getting the MAPE values based on various methods
fcstsku1_mape_SMA<-forecast_SMA_sku1$mape
fcstsku1_mape_ETS<-forecast_ETS_sku1$mape
fcstsku1_mape_arima<-forecast_ARIMA_sku1$mape
fcstsku1_mape_croston<-forecast_Croston_sku1$mape


#Combining all MAPE to one dataframe
fcstsku1_mape<-rbind(fcstsku1_mape_SMA,fcstsku1_mape_ETS,fcstsku1_mape_arima,fcstsku1_mape_croston)

#Writing to File
write.csv(file = "forecast_MAPE_sku1.csv",fcstsku1_mape)



#*****SKU2*****
#Foreasting sku2 using simple moving average, Exponential Smoothing, ARIMA and Croston Method
forecast_SMA_sku2<-Forecast_SMA(sku2)
forecast_ETS_sku2<-Forecast_ETS(sku2)
forecast_ARIMA_sku2<-Forecast_ARIMA(sku2)
#optimal_crost_sku2<-optimal_Croston(sku2) #For finding optimal croston parameters
forecast_Croston_sku2<-Forecast_Croston(sku2,"sbj","mae")


#Getting the actuals and forecasted value dataframes
actualsku2<-forecast_SMA_sku2$actual
predicted_SMA_sku2<-forecast_SMA_sku2$forecast
predicted_ETS_sku2<-forecast_ETS_sku2$forecast
predicted_ARIMA_sku2<-forecast_ARIMA_sku2$forecast
predicted_Croston_sku2<-forecast_Croston_sku2$forecast


#Getting the MAPE values based on various methods
fcstsku2_mape_SMA<-forecast_SMA_sku2$mape
fcstsku2_mape_ETS<-forecast_ETS_sku2$mape
fcstsku2_mape_arima<-forecast_ARIMA_sku2$mape
fcstsku2_mape_croston<-forecast_Croston_sku2$mape


#Combining all MAPE to one dataframe
fcstsku2_mape<-rbind(fcstsku2_mape_SMA,fcstsku2_mape_ETS,fcstsku2_mape_arima,fcstsku2_mape_croston)

#Writing to File
write.csv(file = "forecast_MAPE_sku2.csv",fcstsku2_mape)



#*****SKU3*****
#Foreasting sku3 using simple moving average, Exponential Smoothing, ARIMA and Croston Method
forecast_SMA_sku3<-Forecast_SMA(sku3)
forecast_ETS_sku3<-Forecast_ETS(sku3)
forecast_ARIMA_sku3<-Forecast_ARIMA(sku3)
#optimal_crost_sku3<-optimal_Croston(sku3) #For finding optimal croston parameters
forecast_Croston_sku3<-Forecast_Croston(sku3,"croston","mae")


#Getting the actuals and forecasted value dataframes
actualsku3<-forecast_SMA_sku3$actual
predicted_SMA_sku3<-forecast_SMA_sku3$forecast
predicted_ETS_sku3<-forecast_ETS_sku3$forecast
predicted_ARIMA_sku3<-forecast_ARIMA_sku3$forecast
predicted_Croston_sku3<-forecast_Croston_sku3$forecast


#Getting the MAPE values based on various methods
fcstsku3_mape_SMA<-forecast_SMA_sku3$mape
fcstsku3_mape_ETS<-forecast_ETS_sku3$mape
fcstsku3_mape_arima<-forecast_ARIMA_sku3$mape
fcstsku3_mape_croston<-forecast_Croston_sku3$mape


#Combining all MAPE to one dataframe
fcstsku3_mape<-rbind(fcstsku3_mape_SMA,fcstsku3_mape_ETS,fcstsku3_mape_arima,fcstsku3_mape_croston)

#Writing to File
write.csv(file = "forecast_MAPE_sku3.csv",fcstsku3_mape)

